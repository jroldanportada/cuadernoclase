Este ejercicio es un cuaderno de clase para gestionar los alumnos y sus faltas, así como su calidad en el trabajo, actitud y sus observaciones.

Para realizar este ejercicio he utilizado la API REST de slim, que he modificado en el servidor jesusr.alumno.club.
Dicha API cuenta con una clave (APIKEY) necesaria para acceder a ella mediante esta aplicación y a través de una conexión https.

La aplicación consta de un panel principal que permite gestionar los alumnos o las faltas. Si se elige gestionar alumnos, el profesor podrá añadir, modificar, eliminar o incluso enviarle un e-mail al alumno correspondiente. Si se elige gestionar faltas, el profesor podrá visualizar los alumnos que tienen faltas con su nombre y fecha, y podrá gestionarlos uno a uno haciendo clic sobre cada uno de ellos. Una vez hecho estos cambios, existe un botón flotante que, si es pulsado, almacenará los cambios en la base de datos. Si este botón no se pulsa después de hacer los cambios, éstos no permanecerán en el servidor.

NOTA: Los ficheros presentes en el servidor y la base de datos pueden encontrarse en el directorio "servidor" del repositorio de esta aplicación.